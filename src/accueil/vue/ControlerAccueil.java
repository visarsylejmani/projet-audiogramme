package accueil.vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import reeducasons.vue.VueReeducaSons;

public class ControlerAccueil  implements ActionListener{ 

	private VueReeducaSons vueMain;
		
	public ControlerAccueil (VueReeducaSons vueMain) 
	{
		this.vueMain = vueMain;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		vueMain.update(VueReeducaSons.VUEINSTRUMENT);
	}
}
