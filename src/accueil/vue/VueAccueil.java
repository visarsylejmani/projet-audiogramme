package accueil.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import predefinit.Couleurs;
import predefinit.HintTextFieldUI;
import predefinit.ImageResize;
import predefinit.RoundedBorder;
import reeducasons.vue.VueReeducaSons;

public class VueAccueil extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VueReeducaSons r;
	
	public VueAccueil(VueReeducaSons r){
		this.r = r;
		this.setLayout(new BorderLayout());
		
		//Creation du menu et integration
		JPanel top = createMenu();
		add(top, BorderLayout.NORTH);
		//Creation du panel central et integration
		JPanel center = createCenter();
		add(center, BorderLayout.CENTER);
	}
	
	//creation du menu
	private JPanel createMenu() {
		JPanel res = new JPanel();
		res.setPreferredSize(new Dimension(0,80));
		res.setMinimumSize(getPreferredSize());
		res.setMaximumSize(getPreferredSize());
		res.setBackground(Couleurs.FONDBLOCK);
		
		//
		GridBagLayout gbl = new GridBagLayout();
		res.setLayout(gbl);
		
		JLabel nom = new JLabel(VueReeducaSons.NOMDELAPP);
		nom.setFont(new Font("Calibri", nom.getFont().getStyle(), nom.getFont().getSize() + 25));
		nom.setHorizontalAlignment(SwingConstants.CENTER);
		nom.setForeground(Couleurs.TEXTE);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(20 , 0 , 15 , 0);
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		res.add(nom,gbc);
		
		return res; 
	}
	
	//creation du Panel Central
	private JPanel createCenter() {
		JPanel res = new JPanel();
		res.setBackground(Couleurs.FOND);
		res.setLayout(new GridLayout(1,2));
		res.getLayout();
		res.add(createLeftPanel());
		res.add(createRightPanel());
		
		
		return res;

		
	}
	
	
	//creation du panel gauche (Sons)
		private JPanel createLeftPanel() {
			
			JPanel res = new JPanel();
			res.setBackground(Couleurs.FOND);
			
			res.setLayout(new GridBagLayout());
			
			JPanel inner = new JPanel();
			inner.setLayout(new GridBagLayout());
			inner.setBackground(Couleurs.FONDBLOCK);
			inner.setBorder(new RoundedBorder(Couleurs.FOND, 20));

			JLabel lblSons = new JLabel("Sons");
			lblSons.setFont(new Font("Tahoma", Font.PLAIN, 40));
			lblSons.setForeground(Couleurs.TEXTE);
			GridBagConstraints gbc_lblSons = new GridBagConstraints();
			gbc_lblSons.weightx = 50.0;
			gbc_lblSons.insets = new Insets(20, 0, 5, 0);
			gbc_lblSons.gridx = 0;
			gbc_lblSons.gridy = 0;
			inner.add(lblSons, gbc_lblSons);
			
			JButton buttonInstrument = new JButton("");
			buttonInstrument.addActionListener(new ControlerAccueil(r));
			buttonInstrument.setPreferredSize(new Dimension(90, 90));
			buttonInstrument.setMinimumSize(getPreferredSize());
			buttonInstrument.setMaximumSize(getPreferredSize());
			buttonInstrument.setIcon(new ImageIcon(VueAccueil.class.getResource("/icon/iconTrompette 80px.png")));
			buttonInstrument.setBorder(new RoundedBorder(Couleurs.FONDBLOCK, 20));
			buttonInstrument.setBackground(Couleurs.FONDICON);
			GridBagConstraints gbc_buttonInstrument = new GridBagConstraints();
			gbc_buttonInstrument.gridx = 0;
			gbc_buttonInstrument.gridy = 1;
			inner.add(buttonInstrument, gbc_buttonInstrument);
			
			GridBagConstraints gbc_res = new GridBagConstraints();
			gbc_res.anchor = GridBagConstraints.CENTER;
			gbc_res.fill = GridBagConstraints.BOTH;
			gbc_res.insets = new Insets(10,10,10,5);
			gbc_res.weightx = 1.0;
			gbc_res.weighty = 1.0;
			res.add(inner,gbc_res);
			return res;
		}
	
	
	//creation du panel droit ( Patient )
	private JPanel createRightPanel() {
		JPanel res = new JPanel();
		res.setBackground(Couleurs.FOND);
		
		res.setLayout(new GridBagLayout());
		JPanel inner = new JPanel();
		inner.setBorder(new RoundedBorder(Couleurs.FOND,20));
		inner.setBackground(Couleurs.FONDBLOCK);
		inner.setLayout(new GridBagLayout());
		
		JLabel labelPatient = new JLabel("Patients");
		labelPatient.setForeground(Couleurs.TEXTE);
		labelPatient.setFont(new Font("Tahoma", Font.PLAIN, 40));
		GridBagConstraints gbc_labelPatient = new GridBagConstraints();
		gbc_labelPatient.weightx = 0.2;
		gbc_labelPatient.insets = new Insets(20, 0, 0, 0);
		gbc_labelPatient.ipadx = 1;
		gbc_labelPatient.gridx = 0;
		gbc_labelPatient.gridy = 0;
		inner.add(labelPatient, gbc_labelPatient);
		JButton boutonRecherchePatient = new JButton();
		ImageIcon image = new ImageResize(getClass().getResource("/icon/iconRecherche.png"), 40, 40).resize(); 
		boutonRecherchePatient.setIcon(image);
		boutonRecherchePatient.setBorder(new RoundedBorder(Couleurs.FONDBLOCK,20,new Insets(5, 5, 5, 5)));
		boutonRecherchePatient.setBackground(Couleurs.FONDICON);
		boutonRecherchePatient.setFont(new Font("Tahoma", Font.PLAIN, 15));
		boutonRecherchePatient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				r.update(VueReeducaSons.VUEPATIENT);
			}
		}
		);
		GridBagConstraints gbc_boutonRecherchePatient = new GridBagConstraints();
		gbc_boutonRecherchePatient.weighty = 0.8;
		gbc_boutonRecherchePatient.anchor = GridBagConstraints.LINE_END;
		gbc_boutonRecherchePatient.insets = new Insets(0, 0, 5, 5);
		gbc_boutonRecherchePatient.weightx = Double.MIN_VALUE;
		gbc_boutonRecherchePatient.fill = GridBagConstraints.NONE;
		gbc_boutonRecherchePatient.gridx = 1;
		gbc_boutonRecherchePatient.gridy = 1;
		inner.add(boutonRecherchePatient,gbc_boutonRecherchePatient);
		
		JTextField recherchePatient = new JTextField();
		recherchePatient.setUI(new HintTextFieldUI("      Recherche", true));
		recherchePatient.setBorder(new RoundedBorder(Couleurs.FONDBLOCK,20,new Insets(10, 10, 10, 10)));
		recherchePatient.setBackground(Couleurs.FONDRECHERCHE);
		recherchePatient.setFont(new Font("Tahoma", Font.PLAIN, 15));
		recherchePatient.addKeyListener(new KeyAdapter() {			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if(e.getExtendedKeyCode()==10)r.update(VueReeducaSons.VUEPATIENT);
			}
		});
		GridBagConstraints gbc_recherchePatient = new GridBagConstraints();
		gbc_recherchePatient.weighty = 1.0;
		gbc_recherchePatient.anchor = GridBagConstraints.CENTER;
		gbc_recherchePatient.insets = new Insets(0, 10, 0, 10);
		gbc_recherchePatient.weightx = 1;
		gbc_recherchePatient.fill = GridBagConstraints.HORIZONTAL;
		gbc_recherchePatient.gridx = 0;
		gbc_recherchePatient.gridy = 1;
		inner.add(recherchePatient,gbc_recherchePatient);
		
		GridBagConstraints gbc_panelPatient = new GridBagConstraints();

		gbc_panelPatient.anchor = GridBagConstraints.NORTH;
		gbc_panelPatient.insets = new Insets(20, 10, 20, 20);
		gbc_panelPatient.weighty = 0.5;
		gbc_panelPatient.weightx = 50.0;
		gbc_panelPatient.fill = GridBagConstraints.BOTH;
		gbc_panelPatient.gridx = 1;
		gbc_panelPatient.gridy = 0;
		
		GridBagConstraints gbc_res = new GridBagConstraints();
		gbc_res.anchor = GridBagConstraints.CENTER;
		gbc_res.fill = GridBagConstraints.BOTH;
		gbc_res.insets = new Insets(10,5,10,10);
		gbc_res.weightx = 1.0;
		gbc_res.weighty = 1.0;
		res.add(inner,gbc_res);

		return res;
	}
	
}
