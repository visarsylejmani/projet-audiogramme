package patient.vue;




import javax.swing.JPanel;

import predefinit.Couleurs;
import predefinit.RoundedBorder;

public class VuePatientCenterRight extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2393711979756421510L;

	public VuePatientCenterRight() {
		this.setBorder(new RoundedBorder(Couleurs.FOND, 20));

	}
}
