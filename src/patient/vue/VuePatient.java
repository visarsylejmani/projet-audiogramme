package patient.vue;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import patient.modele.Patient;
import reeducasons.vue.VueReeducaSons;

public class VuePatient extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 * @param vueMain 
	 * @param modelPatient 
	 */
	public VuePatient(VueReeducaSons vueMain, Patient modelPatient) {
		
		
		/*
		 * Le Layout de cette vue
		 */
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{1,1};
		gridBagLayout.rowHeights = new int[]{1,1};
		gridBagLayout.columnWeights = new double[] {1.0};
		gridBagLayout.rowWeights = new double[]{Double.MIN_VALUE,1.0};
		setLayout(gridBagLayout);
		/*
		 * 
		 */
				
		VuePatientTop top = new VuePatientTop(vueMain);
		top.setPreferredSize(new Dimension(getWidth(),80));
		top.setMinimumSize(getPreferredSize());
		top.setMaximumSize(getPreferredSize());
		GridBagConstraints gbc_top = new GridBagConstraints();
		gbc_top.insets = new Insets(0, 0, 0, 0);
		gbc_top.anchor = GridBagConstraints.PAGE_START;
		gbc_top.fill = GridBagConstraints.BOTH;
		gbc_top.gridx = 0;
		gbc_top.gridy = 0;
		add(top, gbc_top);
		
		/*
		 * 
		 */
		
		VuePatientCenter center = new VuePatientCenter();
		GridBagConstraints gbc_center = new GridBagConstraints();
		gbc_center.insets = new Insets(0, 0, 0, 0);
		gbc_center.fill = GridBagConstraints.BOTH;
		gbc_center.anchor = GridBagConstraints.CENTER;
		gbc_center.gridx = 0;
		gbc_center.gridy = 1;
		
		add(center, gbc_center);

	}

}
