package patient.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import predefinit.Couleurs;
import predefinit.JTextFieldRecherche;
import predefinit.RoundedBorder;

public class VuePatientCenterLeft  extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3231194996954732278L;

	public VuePatientCenterLeft() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		
		
		
		// creation d'une barre de recherche 
		
		JTextFieldRecherche recherche = new JTextFieldRecherche();
		recherche.setBorder(new RoundedBorder(getBackground(), 20,new Insets(5, 5, 5, 5)));
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.weightx = Double.MIN_VALUE;
		gbc.weighty = Double.MIN_VALUE;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		this.add(recherche,gbc);
		
		this.setBorder(new RoundedBorder(Couleurs.FOND, 20));
		}

}
