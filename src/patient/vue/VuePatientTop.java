package patient.vue;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import predefinit.Couleurs;
import predefinit.RoundedBorder;
import reeducasons.vue.VueReeducaSons;


public class VuePatientTop  extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VuePatientTop(VueReeducaSons vueMain) {
		this.setBackground(Couleurs.FONDBLOCK);
		
		//
		GridBagLayout gbl = new GridBagLayout();
		gbl.columnWidths = new int[]{0};
		gbl.rowHeights = new int[]{0};
		gbl.columnWeights = new double[]{1.0};
		gbl.rowWeights = new double[]{1.0};
		this.setLayout(gbl);
		
		//Definition du boutton
		JButton button = new JButton();
		ImageIcon i = new ImageIcon(VuePatientTop.class.getResource("/icon/iconVueInstrument 80px.png"));
		button.setIcon(i);
		button.setBackground(Couleurs.FONDICON);
		button.setBorder(new RoundedBorder(getBackground(),20,new Insets(0, 5, 0, 5)));
		//Action du boutton
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				vueMain.update(VueReeducaSons.VUEPRINCIPALE);
			}
		});
		//Placement du boutton
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0, 10, 0, 0);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.gridheight = 0 ;
		gbc.gridwidth = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(button,gbc);
		JLabel nom = new JLabel("Patients");
		nom.setFont(new Font("Calibri", nom.getFont().getStyle(), nom.getFont().getSize() + 25));
		nom.setHorizontalAlignment(SwingConstants.CENTER);
		nom.setForeground(Couleurs.TEXTE);
		gbc.insets = new Insets(20, 0, 15, 0);
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridheight = 0 ;
		gbc.gridwidth = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(nom,gbc);
		

	}
}
