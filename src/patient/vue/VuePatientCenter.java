package patient.vue;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import predefinit.Couleurs;


public class VuePatientCenter extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public VuePatientCenter() {
		GridBagLayout gridBagLayout = new GridBagLayout();		
		this.setLayout(gridBagLayout);
		this.setBackground(Couleurs.FOND);
		
		GridBagConstraints gbcl = new GridBagConstraints();
		gbcl.gridx =0;
		gbcl.gridy =0;		
		gbcl.fill = GridBagConstraints.BOTH;
		gbcl.anchor = GridBagConstraints.CENTER;
		gbcl.weighty = 1;
		gbcl.insets = new Insets(10, 10, 10, 5);
		VuePatientCenterLeft vpl =new VuePatientCenterLeft();
		vpl.setPreferredSize(new Dimension(250,getHeight()));
		vpl.setMinimumSize(getPreferredSize());
		vpl.setMaximumSize(getPreferredSize());
		add(vpl,gbcl);
		GridBagConstraints gbcr = new GridBagConstraints();
		gbcr.gridx =1;
		gbcr.gridy =0;		
		gbcr.fill = GridBagConstraints.BOTH;
		gbcr.anchor = GridBagConstraints.CENTER;
		gbcr.weightx = 1;
		gbcr.weighty = 1;
		gbcr.insets = new Insets(10, 5, 10, 10);
		VuePatientCenterRight vpr =new VuePatientCenterRight();
		add(vpr,gbcr);

		
		

	}

}
