/*
 * Cette classe permet de charger un objet
 */
package sauvegarde;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import predefinit.Systeme;

public class Chargement {
	private static Object donnee = null;
	
	/**
	 * Cette méthode est static pour evite d'instancier la classe
	 * @param nom : correspond au nom du fichier a charger
	 * @return les donnees charger du fichier ouvert
	 */
	public static Object chargement() 
	{
		chargerFichier(new File(Systeme.CHEMIN,Systeme.NOM+Systeme.EXTENSION));
		return donnee;
	}
	
	/**
	 * Charge un fichier
	 * @param fichier: fichier a charger
	 */
	private static void chargerFichier(File fichier) 
	{
		try 
		{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fichier));
			try 
			{
				donnee  = ois.readObject();
			} 
			catch (ClassNotFoundException e) 
			{
				System.err.println("Exception ClassNotFoundException Chargement");
			}
			ois.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println("Exception FileNotFound Chargement");
		} 
		catch (IOException e) 
		{
			System.err.println("IOException Chargement");
		}	
	}
}
