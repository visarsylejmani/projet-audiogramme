/*
 * Cette Action permet de sauvegarder lors d'un appui la classe passer en parametre
 * 
 */

package sauvegarde;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import son.vue.VueSons;

public class SaveBouton implements ActionListener{

	private Object o;
		
	public SaveBouton (Object o) 
	{
		this.o = o;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(o.getClass() == VueSons.class)
		{
			VueSons v = (VueSons) o;
			//Sauvegarde.sauvegarde(Systeme.NOM_INSTRUMENT,v.getModel());
		}
	}
}
