/*
 * Cette classe permet de sauvegarder un objet
 * 
 * 
 */
package sauvegarde;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import predefinit.Systeme;

public class Sauvegarde {
	
	//Cette methode est en static pour eviter d'instancier cette classe
	public static void sauvegarde(Object o) 
	{
		enregistrerSous(Systeme.NOM,Systeme.CHEMIN,Systeme.EXTENSION,o);
	}
	
	//Cette methode permet d'enregistrer un objet
	private static void enregistrerSous(String nom,String chemin,String extension,Object o) 
	{
		try 
		{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(chemin,nom+extension)));
			oos.writeObject(o);
			oos.close();
		} 
		catch (IOException e) 
		{
			System.err.println(e);
		}		
	}
}
