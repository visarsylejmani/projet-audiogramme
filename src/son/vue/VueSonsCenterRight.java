package son.vue;



import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import predefinit.Couleurs;
import predefinit.RoundedBorder;
import son.modele.Son;

public class VueSonsCenterRight extends JPanel 
{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -2393711979756421510L;

	public VueSonsCenterRight(Son son) 
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		VueSonsCenterRightLeft vscrl = new VueSonsCenterRightLeft(son);
		VueSonsListeEnregistrement vccrc = new VueSonsListeEnregistrement(son.getListeDEnregistrement());
		VueSonsCenterRightRight vccrr = new VueSonsCenterRightRight(son.getCourant());

		/*
		 * VueSonsCenterRight
		 */
		
		this.setLayout(gridBagLayout);
		this.setBorder(new RoundedBorder(Couleurs.FOND, 50));
		this.setBackground(Couleurs.FONDPANEL);

		/*
		 * VueSonsCenterRightLeft
		 */
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.weightx = 0;
		//gbc.weightx = 0.3;
		gbc.weighty = 1;
		this.add(vscrl,gbc);
		
		/*
		 * VueSonsCenterRightCenter
		 */
		gbc.weightx = 0.2;
		gbc.insets = new Insets(30, 40, 50, 0);
		this.add(vccrc,gbc);
		
		/*
		 * VueSonsCenterRightRight
		 */
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.weightx = 0.8;
		this.add(vccrr,gbc);
	}
}
