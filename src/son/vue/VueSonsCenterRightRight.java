package son.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import predefinit.Couleurs;
import predefinit.CustomJButtonUI;
import son.modele.Enregistrement;

public class VueSonsCenterRightRight extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean clique = false;
	public VueSonsCenterRightRight(Enregistrement enregistrement)
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbcJ = new GridBagConstraints();
		GridBagConstraints gbc = new GridBagConstraints();
		JButton jl_PlayPause = new JButton();
		JButton jl_Rejouer = new JButton();
		JButton jl_Avancer = new JButton();
		ImageIcon i_Play = new ImageIcon(VueSons.class.getResource("/icon/play_arrow_black_36x36.png"));
		ImageIcon i_Pause = new ImageIcon(VueSons.class.getResource("/icon/pause_black_36x36.png"));
		ImageIcon i_Rejouer = new ImageIcon(VueSons.class.getResource("/icon/replay_black_36x36.png"));
		ImageIcon i_Avancer = new ImageIcon(VueSons.class.getResource("/icon/keyboard_arrow_right_black_36x36.png"));
		VueSonNuageDePoint vsndp = new VueSonNuageDePoint(enregistrement.getnuageDePoint());
		JPanel jp_Jouer = new JPanel();

		
		/*
		 * 
		 */
		
		this.setLayout(gbl);
		this.setOpaque(false);
		
		/*
		 * Panel Nuage De Point
		 */
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(20,20,0,20);
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;		
		this.add(vsndp,gbc);
		
		/*
		 * Panel J
		 * ouer
		 */
		gbc.insets = new Insets(0,0,0,0);
		gbc.fill = GridBagConstraints.NONE;
		gbc.weighty = 0.2;
		gbc.gridy = 1;
		jp_Jouer.setLayout(gbl);
		jp_Jouer.setOpaque(false);
		
		/*
		 * Icone Play Pause
		 */
		
		jl_PlayPause.setIcon(i_Play);
		jl_PlayPause.setUI(new CustomJButtonUI(Couleurs.FONDRECHERCHE,Couleurs.CONTOURRECHERCHE));
		jl_PlayPause.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(clique) jl_PlayPause.setIcon(i_Play);
				else jl_PlayPause.setIcon(i_Pause);
				clique = !clique;
			}
		});
		gbcJ.insets = new Insets(0,30,0,30);
		gbcJ.weightx = 0;
		gbcJ.weighty = 0;
		gbcJ.gridx = 1;
		gbcJ.gridy = 0;		
		jp_Jouer.add(jl_PlayPause,gbcJ);
		
		/*
		 * Icone Rejouer
		 */
		
		jl_Rejouer.setIcon(i_Rejouer);
		jl_Rejouer.setUI(new CustomJButtonUI(Couleurs.FONDRECHERCHE,Couleurs.CONTOURRECHERCHE));
		jl_Rejouer.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{

			}
		});
		gbcJ.gridx = 0;
		gbcJ.insets = new Insets(0,0,0,0);
		jp_Jouer.add(jl_Rejouer,gbcJ);
		
		/*
		 * Icone Avancer
		 */
		
		jl_Avancer.setIcon(i_Avancer);
		jl_Avancer.setUI(new CustomJButtonUI(Couleurs.FONDRECHERCHE,Couleurs.CONTOURRECHERCHE));
		jl_Avancer.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{

			}
		});
		gbcJ.gridx = 2;		
		gbcJ.insets = new Insets(0,0,0,0);
		jp_Jouer.add(jl_Avancer,gbcJ);
		
		this.add(jp_Jouer,gbc);
		
	}

}
