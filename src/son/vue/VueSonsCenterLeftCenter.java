package son.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import predefinit.Couleurs;
import predefinit.CustomJButtonUI;
import predefinit.CustomScrollBarUI;
import predefinit.RoundedBorder;
import son.modele.Son;

public class VueSonsCenterLeftCenter extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VueSonsCenterLeftCenter(ArrayList<Son> list) {
		/*
		 * 
		 */
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		JScrollPane jsp_Liste = new JScrollPane();
		JScrollBar jsb_Verticale;
		JPanel jp_Liste = new JPanel();
		JButton jl_Corps;
		Son le_Son;
		
		/*
		 * VueSonsCenterLeftCenter
		 */
		
		this.setLayout(gridBagLayout);
		this.setOpaque(false);
		
		/*
		 * Creation de la liste des sons à partir d'un model
		 */
	
		jp_Liste.setLayout(gridBagLayout);
		jp_Liste.setBackground(Couleurs.FONDPANEL);
		for (int i = 0; i < list.size(); i++) 
		{
			le_Son = list.get(i);
			jl_Corps = new JButton();
			jl_Corps.setUI(new CustomJButtonUI(Couleurs.FONDBOUTONLIST,Couleurs.CONTOURRECHERCHE));
			jl_Corps.setText(le_Son.getNom());
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.PAGE_START;
			gbc.gridy = i;
			gbc.weightx = 1;
			gbc.weighty = 0.1;
			jp_Liste.add(jl_Corps,gbc);
		}
		jsp_Liste.setViewportView(jp_Liste);
		jsp_Liste.setBorder(new RoundedBorder(Couleurs.FONDPANEL,Couleurs.CONTOURRECHERCHE,1,new Insets(1, 1, 1, 1)));
		jsb_Verticale = jsp_Liste.getVerticalScrollBar();
		jsb_Verticale.setUI(new CustomScrollBarUI());
		jsp_Liste.setVerticalScrollBar(jsb_Verticale);
		jsp_Liste.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jsp_Liste.setOpaque(false);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1;
		gbc.weighty = 1;
		this.add(jsp_Liste,gbc);
	}

}
