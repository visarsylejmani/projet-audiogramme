package son.vue;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import predefinit.Couleurs;
import son.modele.LesSons;


public class VueSonsCenter extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VueSonsCenter(LesSons modelSon) 
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		VueSonsCenterLeft vscl = new VueSonsCenterLeft(modelSon);
		VueSonsCenterRight vccr = new VueSonsCenterRight(modelSon.getCourant());

		/*
		 * VueSonsCenter
		 */
		
		this.setLayout(gridBagLayout);
		this.setBackground(Couleurs.FOND);
		
		/*
		 * VueSonsCenterLeft
		 */
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1;
		gbc.insets = new Insets(20, 20, 20, 10);
		this.add(vscl,gbc);
		
		/*
		 * VueSonsCenterRight
		 */
		
		gbc.insets = new Insets(20, 10, 20, 20);
		gbc.weightx = 0.7;
		this.add(vccr,gbc);
	}
}
