package son.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import reeducasons.vue.VueReeducaSons;
import son.modele.LesSons;

@SuppressWarnings("deprecation")
public class VueSons extends JPanel implements Observer
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -213817307774611381L;

	/**
	 * Create the panel.
	 * @param vueMain 
	 * @param modelSon 
	 */
	public VueSons(VueReeducaSons vueMain, LesSons modelSon) 
	{
		
		/*
		 * 
		 */		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints gbc_top = new GridBagConstraints();
		GridBagConstraints gbc_center = new GridBagConstraints();
		VueSonsTop vueSonsTop = new VueSonsTop(vueMain);
		VueSonsCenter vueSonsCenter = new VueSonsCenter(modelSon);
				
		/*
		 * VueSons
		 */
		
		gridBagLayout.columnWidths = new int[]{1,1};
		gridBagLayout.rowHeights = new int[]{1,1};
		gridBagLayout.columnWeights = new double[] {1.0};
		gridBagLayout.rowWeights = new double[]{Double.MIN_VALUE,1.0};
		this.setLayout(gridBagLayout);
		
		/*
		 * VueSonTop
		 */		

		gbc_top.insets = new Insets(0, 0, 0, 0);
		gbc_top.fill = GridBagConstraints.BOTH;
		gbc_top.anchor = GridBagConstraints.PAGE_START;
		gbc_top.gridx = 0;
		gbc_top.gridy = 0;
		this.add(vueSonsTop, gbc_top);
		
		/*
		 * VueSonsCenter
		 */
		
		gbc_center.insets = new Insets(0, 0, 0, 0);
		gbc_center.fill = GridBagConstraints.BOTH;
		gbc_center.anchor = GridBagConstraints.CENTER;
		gbc_center.gridx = 0;
		gbc_center.gridy = 1;
		this.add(vueSonsCenter, gbc_center);
	}

	@Override
	public void update(Observable o, Object arg) 
	{
		
	}
}
