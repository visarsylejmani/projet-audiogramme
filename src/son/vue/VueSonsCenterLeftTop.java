package son.vue;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import predefinit.Couleurs;
import predefinit.HintTextFieldUI;
import predefinit.RoundedBorder;
import son.modele.LesSons;

public class VueSonsCenterLeftTop extends JPanel
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VueSonsCenterLeftTop(LesSons modelSon) 
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		JTextField jtf_Recherche = new JTextField();
		JPanel jp_CheckBox = new JPanel();
		JCheckBox jcb_Instrument = new JCheckBox("Instruments");
		JCheckBox jcb_Bruits = new JCheckBox("Bruits");
		JCheckBox jcb_Voix = new JCheckBox("Voix");
		ImageIcon i_DisableSelected = new ImageIcon(VueSons.class.getResource("/icon/check_box_outline_blank_grey_18x18.png"));
		ImageIcon i_Selected = new ImageIcon(VueSons.class.getResource("/icon/check_box_grey_18x18.png"));

		/*
		 * 
		 */
		
		this.setLayout(gbl);
		this.setOpaque(false);
		
		/*
		 * Barre de Recherche
		 */
		
		jtf_Recherche.setUI(new HintTextFieldUI("   Recherche", true));
		jtf_Recherche.setBorder(new RoundedBorder(Couleurs.FONDPANEL,Couleurs.CONTOURRECHERCHE,20,new Insets(5, 5, 5, 5)));
		jtf_Recherche.setBackground(Couleurs.FONDRECHERCHE);
		jtf_Recherche.setFont(new Font("Tahoma", Font.PLAIN, 10));
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.insets = new Insets(10, 5, 0, 5);
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;		
		this.add(jtf_Recherche,gbc);
		
		/*
		 * Panel des CheckBoxs
		 */
		
		jp_CheckBox.setLayout(gbl);
		jp_CheckBox.setOpaque(false);
		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.insets = new Insets(0, 0, 0, 0);	
		gbc.gridy = 1;
		this.add(jp_CheckBox,gbc);

		/*
		 * CheckBox Instruments
		 */
		//Exemple Manque le modele
		ActionListener al_Checkbox = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(jcb_Instrument.isSelected())
					jcb_Instrument.setIcon(i_Selected);
				else
					jcb_Instrument.setIcon(i_DisableSelected);
			}
		};

		jcb_Instrument.setIcon(i_DisableSelected);
		jcb_Instrument.addActionListener(al_Checkbox);
		jcb_Instrument.setFont(new Font("Calibri", jcb_Instrument.getFont().getStyle(), jcb_Instrument.getFont().getSize()));
		jcb_Instrument.setBackground(Couleurs.FONDPANEL);
		jcb_Instrument.setForeground(Couleurs.CHEKBOXTEXT);
		jp_CheckBox.add(jcb_Instrument);
		
		/*
		 * CheckBox Bruits
		 */
		
		jcb_Bruits.setIcon(i_DisableSelected);
		jcb_Bruits.setFont(new Font("Calibri", jcb_Bruits.getFont().getStyle(), jcb_Bruits.getFont().getSize()));
		jcb_Bruits.setBackground(Couleurs.FONDPANEL);
		jcb_Bruits.setForeground(Couleurs.CHEKBOXTEXT);
		jp_CheckBox.add(jcb_Bruits);
		
		/*
		 * CheckBox Bruits
		 */
		
		jcb_Voix.setIcon(i_DisableSelected);
		jcb_Voix.setFont(new Font("Calibri", jcb_Voix.getFont().getStyle(), jcb_Voix.getFont().getSize()));
		jcb_Voix.setBackground(Couleurs.FONDPANEL);
		jcb_Voix.setForeground(Couleurs.CHEKBOXTEXT);
		jp_CheckBox.add(jcb_Voix);
	}
}
