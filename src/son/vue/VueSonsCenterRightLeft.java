package son.vue;


import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import predefinit.Couleurs;
import predefinit.CustomJButtonUI;
import predefinit.RoundedBorder;
import son.modele.Son;

public class VueSonsCenterRightLeft extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VueSonsCenterRightLeft(Son son)
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		ImageIcon i_ImageSon = new ImageIcon(VueSons.class.getResource(son.getImage()));
		JLabel jl_ImageSon = new JLabel();
		JLabel jl_NomSon = new JLabel("Son");
		JButton jb_ModifierSon = new JButton("Modifier");
		//VueSonsListeEnregistrement vccrc = new VueSonsListeEnregistrement(son.getListeDEnregistrement());
		
		/*
		 * 
		 */
		
		this.setLayout(gbl);
		this.setOpaque(false);
		
		/*
		 * Icone du son
		 */
		
		jl_ImageSon.setIcon(i_ImageSon);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.insets = new Insets(10, 0, 0, 0);
		gbc.gridx = 0;
		gbc.gridy = 0;		
		this.add(jl_ImageSon,gbc);
		
		/*
		 * Label Son
		 */
		
		jl_NomSon.setFont(new Font("Calibri", jl_NomSon.getFont().getStyle(), jl_NomSon.getFont().getSize() + 40));
		gbc.insets = new Insets(0, 40, 0, 0);
		//gbc.fill = GridBagConstraints.NONE;
		//gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.FIRST_LINE_END;
		gbc.gridy = 1;		
		this.add(jl_NomSon,gbc);
		
		/*
		 * Bouton Modifier
		 */
		
		jb_ModifierSon.setBackground(Couleurs.FONDPANEL);
		jb_ModifierSon.setForeground(Couleurs.CHEKBOXTEXT);
		jb_ModifierSon.setUI(new CustomJButtonUI(Couleurs.FONDRECHERCHE,Couleurs.CONTOURRECHERCHE));
		jb_ModifierSon.setBorder(new RoundedBorder(Couleurs.FONDPANEL,Couleurs.CONTOURRECHERCHE,20,new Insets(5, 5, 5, 5)));
		gbc.insets = new Insets(15, 0, 0, 0);
		//gbc.anchor = GridBagConstraints.FIRST_LINE_END;
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.gridy = 1;
		gbc.gridx = 1;
		this.add(jb_ModifierSon,gbc);
		
		
		/*
		 * Bonus Liste dessous
		 */
		/*
		 * VueSonsCenterRightCenter
		 */
		/*gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridy = 2;
		gbc.insets = new Insets(30, 40, 50, 0);
		this.add(vccrc,gbc);*/
	}
}
