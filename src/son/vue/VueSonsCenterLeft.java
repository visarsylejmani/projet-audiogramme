package son.vue;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import predefinit.Couleurs;
import predefinit.RoundedBorder;
import son.modele.LesSons;


public class VueSonsCenterLeft  extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3231194996954732278L;

	public VueSonsCenterLeft(LesSons modelSon) 
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		VueSonsCenterLeftTop vsclt = new VueSonsCenterLeftTop(modelSon);
		VueSonsCenterLeftCenter vcclc = new VueSonsCenterLeftCenter(modelSon.getListeSons());

		/*
		 * VueSonsCenterLeft
		 */
		
		this.setLayout(gridBagLayout);
		this.setBorder(new RoundedBorder(Couleurs.FOND, 50));
		this.setBackground(Couleurs.FONDPANEL);
		this.setPreferredSize(new Dimension(250,0));
		this.setMinimumSize(this.getPreferredSize());
		this.setMaximumSize(this.getPreferredSize());

		/*
		 * VueSonsCenterLeftTop
		 */
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.insets = new Insets(0, 20, 0, 20);
		this.add(vsclt,gbc);
		
		/*
		 * VueSonsCenterLeftCenter
		 */
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 0, 30, 5);
		gbc.weighty = 1;
		this.add(vcclc,gbc);
	}
}
