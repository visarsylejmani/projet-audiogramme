package son.vue;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import predefinit.Couleurs;
import predefinit.CustomJButtonUI;
import predefinit.RoundedBorder;
import reeducasons.vue.VueReeducaSons;


public class VueSonsTop  extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VueSonsTop(VueReeducaSons vueMain) 
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		JLabel jl_Nom = new JLabel("Sons");
		JButton jb_Retour = new JButton();
		JButton jb_CreeSon = new JButton();
		ImageIcon i_Retour = new ImageIcon(VueSonsTop.class.getResource("/icon/iconVueInstrument 80px.png"));
		ImageIcon i_CreeSon = new ImageIcon(VueSonsTop.class.getResource("/icon/iconTrompette 80px.png"));
		
		/*
		 * VueSonsTop
		 */
		
		gbl.columnWidths = new int[]{0};
		gbl.rowHeights = new int[]{0};
		gbl.columnWeights = new double[]{1.0};
		gbl.rowWeights = new double[]{1.0};
		this.setLayout(gbl);
		this.setBackground(Couleurs.FONDBLOCK);
		
		/*
		 * Bouton Retour
		 */
		
		jb_Retour.setIcon(i_Retour);
		jb_Retour.setUI(new CustomJButtonUI(Couleurs.FONDICON,Couleurs.FONDICON));
		jb_Retour.setBackground(Couleurs.FONDICON);
		jb_Retour.setBorder(new RoundedBorder(getBackground(),20,new Insets(0, 5, 0, 5)));

		jb_Retour.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				vueMain.update(VueReeducaSons.VUEPRINCIPALE);
			}
		});

		gbc.insets = new Insets(0, 10, 0, 0);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.gridheight = 0 ;
		gbc.gridwidth = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(jb_Retour,gbc);
		
		/*
		 * Label Nom de l'application
		 */
		
		jl_Nom.setFont(new Font("Calibri", jl_Nom.getFont().getStyle(), jl_Nom.getFont().getSize() + 25));
		jl_Nom.setHorizontalAlignment(SwingConstants.CENTER);
		jl_Nom.setForeground(Couleurs.TEXTE);
		gbc.insets = new Insets(20, 0, 15, 0);
		gbc.anchor = GridBagConstraints.CENTER;
		this.add(jl_Nom,gbc);
		
		/*
		 * Bouton Cree un sons
		 */
		
		jb_CreeSon.setIcon(i_CreeSon);
		jb_CreeSon.setUI(new CustomJButtonUI(Couleurs.FONDICON,Couleurs.FONDICON));
		jb_CreeSon.setBackground(Couleurs.FONDICON);
		jb_CreeSon.setPreferredSize(new Dimension(85, 75));
		jb_CreeSon.setMinimumSize(new Dimension(0, 0));
		jb_CreeSon.setMaximumSize(new Dimension(0, 0));
		jb_CreeSon.setBorder(new RoundedBorder(getBackground(),20,new Insets(0, 5, 0, 5)));

		jb_CreeSon.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{

			}
		});
		
		gbc.insets = new Insets(0, 0, 0, 10);
		gbc.anchor = GridBagConstraints.LINE_END;
		this.add(jb_CreeSon,gbc);
	}
}
