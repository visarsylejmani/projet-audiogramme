package son.modele;

import java.io.Serializable;
import java.util.ArrayList;

import recherche.Recherche;

public class LesSons implements Recherche,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4475427457219832914L;
	private ArrayList<Son> listeSons = new ArrayList<Son> ();
    private Son courant = new Son();

    public Son getCourant() {
		return courant;
	}

	public void setCourant(Son courant) {
		this.courant = courant;
	}

	public ArrayList<Son> getListeSons() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.listeSons;
    }

    public void setListeSons(ArrayList<Son> value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.listeSons = value;
    }
}
