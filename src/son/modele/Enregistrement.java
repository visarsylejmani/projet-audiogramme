package son.modele;

import java.io.Serializable;
import java.util.Date;

public class Enregistrement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8805723602986733298L;

	private Date date;

    private String nom;

    private String audio;

    private Nuage nuageDePoint = new Nuage();

    Date getDate() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.date;
    }

    void setDate(Date value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.date = value;
    }

    public String getNom() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nom;
    }

    public void setNom(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nom = value;
    }

    public String getAudio() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.audio;
    }

    void setAudio(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.audio = value;
    }

    public Nuage getnuageDePoint() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nuageDePoint;
    }

    void setnuageDePoint(Nuage value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nuageDePoint = value;
    }

}
