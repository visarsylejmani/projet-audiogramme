package son.modele;

import java.io.Serializable;
import java.util.ArrayList;

public class Son implements Serializable {

	private static final long serialVersionUID = 5474770755761286860L;

	/**
	 * 
	 */


	private String nom = "defaut";

    private TypeSon type = TypeSon.bruit;

    private String image = "";

    private ArrayList<Enregistrement> listeDEnregistrement = new ArrayList<Enregistrement> ();

    private Enregistrement courant = new Enregistrement();
    public Enregistrement getCourant() {
		return courant;
	}

	public void setCourant(Enregistrement courant) {
		this.courant = courant;
	}

	public String getNom() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nom;
    }

    public void setNom(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nom = value;
    }

    public TypeSon getType() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.type;
    }

    public void setType(TypeSon value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.type = value;
    }

    public String getImage() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.image;
    }

    public void setImage(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.image = value;
    }

    public ArrayList<Enregistrement> getListeDEnregistrement() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.listeDEnregistrement;
    }

    public void setListeDEnregistrement(ArrayList<Enregistrement> value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.listeDEnregistrement = value;
    }

}
