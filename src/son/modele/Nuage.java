package son.modele;

import java.io.Serializable;
import java.util.ArrayList;

import coordonneees.Coordonnees;

public class Nuage implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6396950962910006941L;
	private ArrayList<Coordonnees> listeDeCoordonnees = new ArrayList<Coordonnees> ();

    public ArrayList<Coordonnees> getListeDeCoordonnees() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.listeDeCoordonnees;
    }

    public void setListeDeCoordonnees(ArrayList<Coordonnees> value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.listeDeCoordonnees = value;
    }

}
