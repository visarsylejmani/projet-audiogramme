package predefinit;

import java.awt.Graphics;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.beans.ConstructorProperties;

import javax.swing.border.AbstractBorder;


/**
 * @author Visar Sylejmani
 */
@SuppressWarnings("serial") // Same-version serialization only
public class RoundedBorder extends AbstractBorder
{
	/**
	 * 
	 */
    protected Color backgroundColor;
    
    protected Color contourColor = null;
    
	protected int arc;

	protected Insets insets;


    /**
     * Create a border with a specific color and an arc
     * @param backgroundColor
     * @param arc
     */
	
    @ConstructorProperties({"backgroundColor"})
	public RoundedBorder(Color backgroundColor)  {
    	this(backgroundColor,backgroundColor,10,new Insets(0, 0, 0, 0));
        
    }
    @ConstructorProperties({"backgroundColor", "arc"})
    public RoundedBorder(Color backgroundColor, int arc)  {
    	this(backgroundColor,backgroundColor,arc,new Insets(0, 0, 0, 0));
        
    }

    @ConstructorProperties({"backgroundColor", "arc", "insets"})
    public RoundedBorder(Color backgroundColor, int arc,Insets insets)  {
    	this(backgroundColor,backgroundColor,arc,insets);
        }
    @ConstructorProperties({"backgroundColor","contourColor","arc", "insets",})
    public RoundedBorder(Color backgroundColor,Color contourColor, int arc,Insets insets)  {
    	this.backgroundColor  = backgroundColor;
        this.arc = arc;
        this.insets = insets;
        this.contourColor = contourColor;

        }
	/**
     *
     * @param c the component for which this border is being painted
     * @param g the paint graphics
     * @param x the x position of the painted border
     * @param y the y position of the painted border
     * @param width the width of the painted border
     * @param height the height of the painted border
     */ 
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
   	 Graphics2D g2d = (Graphics2D) g;

        Color oldColor = g2d.getColor();
        g2d.setColor(this.backgroundColor);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        Shape outer;
        Shape inner;

        outer = new Rectangle2D.Float(x, y, width, height);
        inner = new RoundRectangle2D.Float(x , y , width , height, arc, arc);
        Path2D path = new Path2D.Float(Path2D.WIND_EVEN_ODD);
  
        
        path.append(outer, false);
        path.append(inner, true);
        g2d.fill(path);
        
        Shape mid;
            
        g2d.setColor(this.contourColor);

        mid = new RoundRectangle2D.Float(x , y , width-1 , height-1, arc, arc);

        Path2D path2 = new Path2D.Float(Path2D.WIND_EVEN_ODD);
      
        path2.append(mid, false);
        g2d.draw(path2);
            
        g2d.setColor(oldColor);
   }
    /**
     * Reinitialize the insets parameter with this Border's current Insets.
     *
     * @param c the component for which this border insets value applies
     * @param insets the object to be reinitialized
     */
    public Insets getBorderInsets(Component c, Insets insets) {
    	insets.set(this.insets.top, this.insets.left, this.insets.bottom, this.insets.right);
        return insets;
    }

    public boolean isBorderOpaque() {
        return false;
    }

}
