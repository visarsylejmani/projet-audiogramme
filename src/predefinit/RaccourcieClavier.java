package predefinit;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import reeducasons.model.ReeducaSons;
import sauvegarde.Sauvegarde;

public class RaccourcieClavier extends AbstractAction
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 727703975075761899L;
	private ReeducaSons rs;
	
	public RaccourcieClavier (ReeducaSons rs) 
	{
		this.rs = rs;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Sauvegarde.sauvegarde(rs);
	}

}