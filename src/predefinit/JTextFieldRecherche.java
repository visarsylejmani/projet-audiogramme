package predefinit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;

public class JTextFieldRecherche  extends JTextField{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8112410952783023476L;

	public JTextFieldRecherche() {
		this.setUI(new HintTextFieldUI("      Recherche", true));
		this.setLayout(new BorderLayout());
		JButton button = new JButton();
		button.setBackground(Couleurs.FONDBOUTON);
		button.setPreferredSize(new Dimension(15,15));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		ImageIcon image = new ImageResize(getClass().getResource("/icon/iconRecherche.png"), button.getPreferredSize().width	, button.getPreferredSize().height).resize(); 
		button.setIcon(image);
		button.setBackground(Color.WHITE);
		button.setBorder(new RoundedBorder(button.getBackground(),20));
		
		button.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
			}
		});
		button.setBackground(this.getBackground());
		this.setMargin(new Insets(2, 2, 2, 20));
		
		this.add(button,BorderLayout.EAST);

	}
}
