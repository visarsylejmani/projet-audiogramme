package predefinit;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.beans.ConstructorProperties;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicButtonUI;

public class CustomJButtonUI extends BasicButtonUI {
	private Color contourColor;
	private Color backgroundColor;
	
    @ConstructorProperties({"backgroundColor", "arc"})
    public CustomJButtonUI(Color backgroundColor)  {
    	this(backgroundColor,backgroundColor);
    }
    
    public CustomJButtonUI(Color backgroundColor,Color contourColor)  {
    	this.backgroundColor  = backgroundColor;
        this.contourColor = contourColor;
    }

    @Override
    public void installUI (JComponent c) {
        super.installUI(c);
        AbstractButton button = (AbstractButton) c;
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(5, 15, 5, 15));
    }

    @Override
    public void paint (Graphics g, JComponent c) {
        AbstractButton b = (AbstractButton) c;
        paintBackground(g, b, b.getModel().isPressed() ? false : true);
        super.paint(g, c);
    }

    private void paintBackground (Graphics g, JComponent c, boolean pressed) {
        Dimension size = c.getSize();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(backgroundColor);
        if(!pressed) g.setColor(c.getBackground().darker());
        g.fillRoundRect(0, 0, size.width, size.height, 0, 0);
        g.setColor(contourColor);
        g.drawRoundRect(0, 0, size.width, size.height, 0, 0);
    }
}