package predefinit;

import java.awt.Image;
import java.beans.ConstructorProperties;
import java.net.URL;

import javax.swing.ImageIcon;

public class ImageResize {
	private ImageIcon res;
	@ConstructorProperties({"url","width","height"})
	public ImageResize(URL url, int width,int height) {
		this.res = new ImageIcon(url);
		Image newImage = this.res.getImage();
		Image modifiedImage = newImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		this.res = new ImageIcon(modifiedImage);
	}
	
	public ImageIcon resize() {
		return this.res;
		
	}
}
