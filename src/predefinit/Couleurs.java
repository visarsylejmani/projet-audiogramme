package predefinit;

import java.awt.Color;

public class Couleurs {
	public static Color FONDBLOCK = new Color(255,173,45);
	public static Color FOND = new Color(52,38,15);
	public static Color TEXTE = new Color(255,255,255);
	public static Color FONDBOUTON = new Color(255,255,255);
	public static Color FONDBOUTONLIST= new Color(255,248,248);
	public static Color FONDRECHERCHE = new Color(255,255,255);
	public static Color FONDICON = new Color(255,191,91);
	public static Color FONDPANEL = new Color(255,255,255);
	public static Color CONTOURRECHERCHE = new Color(220,220,220);
	public static Color CHEKBOXTEXT = new Color(150,150,150);
}
