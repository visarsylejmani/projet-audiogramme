package reeducasons.vue;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import accueil.vue.VueAccueil;
import patient.modele.Patient;
import patient.vue.VuePatient;
import predefinit.RaccourcieClavier;
import reeducasons.model.ReeducaSons;
import sauvegarde.Chargement;
import sauvegarde.Sauvegarde;
import son.vue.VueSons;


public class VueReeducaSons {
	public final static String VUEPRINCIPALE = "VuePrincipale";
	public final static String VUEINSTRUMENT = "VueInstrument";
	public final static String VUEPATIENT = "VuePatient";
	public final static String NOMDELAPP = "Rééduca'Sons";

	private ReeducaSons modelReeducaSon = new ReeducaSons();
	private JFrame frame = new JFrame();
	private CardLayout cards = new CardLayout();
	private JPanel pane = new JPanel();
	private Patient modelPatient = new Patient();
	private VueSons vueSon;
	private VuePatient vuePatient = new VuePatient(this,modelPatient);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VueReeducaSons window = new VueReeducaSons();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VueReeducaSons() {
		chargement();
		initialize();
		creatpane();
	}

	private void chargement() 
	{
		Object charger = Chargement.chargement();
		if(charger != null) modelReeducaSon = (ReeducaSons) charger;
		else Sauvegarde.sauvegarde(modelReeducaSon);

	}

	private void creatpane() 
	{
		pane.setLayout(cards);
		pane.add(new VueAccueil(this),VUEPRINCIPALE);
		pane.add(vuePatient,VUEPATIENT);
		pane.add(vueSon,VUEINSTRUMENT);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame.setBounds(new Rectangle(new Dimension(1024,576)));
		frame.setTitle("Rééduca'Sons");
		frame.setLocation(100,100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(1024,576));
		frame.setContentPane(pane);
		
		String KEY = "ctrS";
		InputMap im = pane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK),KEY);
		pane.getActionMap().put(KEY, new RaccourcieClavier(modelReeducaSon));

		vueSon = new VueSons(this,modelReeducaSon.getLesSons());

	}
	public void update(String choix) 
	{
		switch (choix) {
		case VUEPRINCIPALE:
			cards.show(pane, VUEPRINCIPALE);
			break;
		case VUEPATIENT :
			cards.show(pane, VUEPATIENT);
			break;
		case VUEINSTRUMENT:
			cards.show(pane, VUEINSTRUMENT);
			break;
	//	case 2:
		//	break;
		}
	}
}
