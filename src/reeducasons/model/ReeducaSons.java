package reeducasons.model;

import java.io.Serializable;
import java.util.ArrayList;

import son.modele.Enregistrement;
import son.modele.LesSons;
import son.modele.Son;
import son.modele.TypeSon;

public class ReeducaSons implements Serializable {
   // private LesPatients lesPatientsArchives;

    //private LesPatients lesPatients;
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LesSons lesSons = new LesSons();
    public ReeducaSons()
    {
		/*
		 * TEST
		 */
    	LesSons modelSon = new LesSons();
		Son teste;
		Enregistrement testeE;
		ArrayList<Son> listeSons = new ArrayList<Son> ();
		ArrayList<Enregistrement> listEnregistrement;
		for (int k = 0; k < 1; k++) 
		{
			listEnregistrement = new ArrayList<Enregistrement> ();
			for (int j = 0; j < 100; j++) 
			{
				testeE = new Enregistrement();
				testeE.setNom("Enregistrement n°"+j);
				listEnregistrement.add(testeE);
			}
			teste = new Son();
			teste.setListeDEnregistrement(listEnregistrement);
			teste.setCourant(listEnregistrement.get(1));
			teste.setNom("Son n°"+k);
			teste.setImage("/icon/donut_large_black_144x144.png");
			teste.setType(TypeSon.bruit);
			listeSons.add(teste);
		}
		modelSon.setCourant(listeSons.get(0));
		modelSon.setListeSons(listeSons);
		
		setLesSons(modelSon);
    }
   /* LesPatients getLesPatients() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.lesPatients;
    }*/

   /* void setLesPatients(LesPatients value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.lesPatients = value;
    }*/

    /*LesPatients getLesPatientsArchives() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.lesPatientsArchives;
    }*/

    /*void setLesPatientsArchives(LesPatients value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.lesPatientsArchives = value;
    }*/

    public LesSons getLesSons() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.lesSons;
    }

    public void setLesSons(LesSons value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.lesSons = value;
    }
}
