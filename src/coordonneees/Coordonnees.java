package coordonneees;

import java.io.Serializable;

public class Coordonnees implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5857567391018891486L;
	private int Frequence;
    private int Intensite;

    int getFrequence() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.Frequence;
    }

    void setFrequence(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.Frequence = value;
    }

    int getIntensite() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.Intensite;
    }

    void setIntensite(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.Intensite = value;
    }

}
